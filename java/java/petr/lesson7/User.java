package petr.lesson7;

public class User {
    private String firstName;
    private String lastName;
    private int age;
    private boolean isWorker;

    private User(Builder builder){
        this.firstName = builder.firstName;
        this.lastName = builder.lastName;
        this.age = builder.age;
        this.isWorker = builder.isWorker;
    }
    public String getFirstName(){
        return firstName;
    }
    public String getLastName(){
        return lastName;
    }
    public int getAge(){
        return age;
    }
    public boolean getIsWorker(){
        return isWorker;
    }
    public static Builder builder (){
        return new Builder();
    }



    public static class Builder {
        String firstName = "";
        String lastName = "";
        int age = 0;
        boolean isWorker = false;

        public Builder(){
        }

        public Builder firstName(String firstName) {
            this.firstName = firstName;
            return this;
        }

        public Builder lastName(String lastName) {
            this.lastName = lastName;
            return this;
        }

        public Builder age(int age) {
            this.age = age;
            return this;
        }

        public Builder isWorker(boolean isWorker) {
            this.isWorker = isWorker;
            return this;
        }

        public User build() {
            return new User(this);
        }

    }


}
