package petr.lesson1.task2;

import java.util.Scanner;

class Program2 {
    public static void main(String args[]) {
        Scanner scanner = new Scanner(System.in);

        long pr = 1;

        int num = scanner.nextInt();

        while (num != 0) {
            int permNum = num;
            int currentNum = 0;
            while (permNum != 0) {
                currentNum = currentNum + (permNum % 10);
                permNum = permNum / 10;
            }

            if (currentNum % 2 != 0 && currentNum % 3 != 0 && currentNum % 5 != 0 && currentNum % 7 != 0) {
                pr = pr * num;
            }
            num = scanner.nextInt();

        }

        System.out.println(pr);
    }
}
