package petr.lesson1.task;

class Program2 {
    public static void main(String args[]) {
        int number = 12345;
        int digitsSum = (number / 10000);

        digitsSum = digitsSum + (number / 1000) % 10;

        digitsSum = digitsSum + (number / 100) % 10;

        digitsSum = digitsSum + (number / 10) % 10;

        digitsSum = digitsSum + number % 10;

        System.out.println(digitsSum);

    }
}
