package petr.lesson1.task3;

import java.util.Scanner;

class Program {
public static void main(String args[]) {
Scanner scanner = new Scanner(System.in);

long product = 1;

int number = scanner.nextInt();

while (number != 0) 
{ 
	int permanentNumber = number;
	int sumOfNumbers = 0;

	while (permanentNumber != 0) 
	{
	sumOfNumbers = sumOfNumbers + (permanentNumber % 10);
	permanentNumber = permanentNumber / 10;
	}

	for (int i = 2; i < sumOfNumbers; i++)
	{
		if (sumOfNumbers % i == 0)
		{
		number = 1;
		}
	}
	product = product * number;
	number = scanner.nextInt();				
}

System.out.println(product);
}
}
