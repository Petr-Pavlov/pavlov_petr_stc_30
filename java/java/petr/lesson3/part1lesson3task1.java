package petr.lesson3;

import java.util.Arrays;
import java.util.Scanner;
class Part1Lesson3Task1 {
	public static void main(String args[]) {
		Scanner scanner = new Scanner(System.in);

		System.out.print("Введите размер массива ");
		int n = scanner.nextInt();
		int array[] = new int[n];

		System.out.println("Введите " + array.length + " чисел для заполнения массива");
		for (int i = 0; i < array.length; i++){
			array[i] = scanner.nextInt();
		}

		System.out.println("Выберите требуемое действие: \n" + 
			"1 - вычислить сумму элементов массива \n" +
			"2 - выполнить разворот массива \n" +
			"3 - вычислить среднее арифметическое массива \n" +
			"4 - поменять местами максимальный и минимальный элементы массива \n" +
			"5 - выполнить сортировку массива \n" +
			"6 - преобразовать массив в число");

		int actionChoise = scanner.nextInt();

		if(actionChoise == 1){
			arraySum(array);
		}
		else if(actionChoise == 2){
			reversalArray(array);
		}
		else if(actionChoise == 3){
			arrayAverage(array);
		}
		else if(actionChoise == 4){
			changingMinAndMaxElements(array); 
		}
		else if(actionChoise == 5){
			sortedArray(array);
		}
		else if(actionChoise == 6){
			convertArrayToNumber(array);
		}
		else {
			System.out.println("некорректно выбрано действие");
		}
	}

	public static void arraySum(int[] array) {
		int arraySum = 0;
		for (int i = 0; i < array.length; i++){
			arraySum = arraySum + array[i];
		}

		System.out.println(arraySum);
	}

	public static void reversalArray(int[] array) {
		
		int reversalArray[] = new int[array.length];
		
		int j = 0;

		for (int i = array.length - 1; i >= 0; i--) {
			reversalArray[j] = array [i];
			j++;
		}
		
		System.out.println(Arrays.toString(reversalArray));
	}

	public static void arrayAverage(int[] array) {
		
		int arraySum = 0;
		for (int i = 0; i < array.length; i++) {
			arraySum = arraySum + array[i];
		}

		int arrayAverage = arraySum / array.length;
		System.out.println(arrayAverage);
	}

	public static void changingMinAndMaxElements(int[] array) {
		
		int arrayMin = array[0];
		int arrayMax = array[0];
		int indexOfMin = 0;
		int indexOfMax = 0;

		for (int i = 1; i < array.length; i++){
			if (arrayMin > array[i]){
				arrayMin = array[i];
				indexOfMin = i;
			}
		}

		for (int i = 1; i < array.length; i++){
			if (arrayMax < array[i]){
				arrayMax = array[i];
				indexOfMax = i;
			}
		}

		int temp = array[indexOfMax];
		array[indexOfMax] = array[indexOfMin];
		array[indexOfMin] = temp;


		System.out.println(Arrays.toString(array));
	}

	public static void sortedArray(int[] array) {
		
		int temp = 0;
		boolean sorted = false;

		while (sorted == false){
			sorted = true;
			
			for (int i = 0; i < array.length - 1; i++){
				int j = i + 1;
				if (array[i] > array[j]){
					temp = array[j];
					array[j] = array[i];
					array[i] = temp;
					sorted = false;
				}
			}
		}

		System.out.println(Arrays.toString(array));
	}

	public static void convertArrayToNumber(int[] array) {
		
		long number = 0;
		long numberRank = 1;
		
		for (int i = array.length - 1; i >= 0; i--){
			number = number + array[i] * numberRank;
			int temp = array[i];
			
			while (temp != 0){
				temp = temp / 10;
				numberRank = numberRank * 10;
			}
		}

		System.out.println(number); 
	}
	
}