package petr.lesson11;

public interface Iterable<T> {

    Iterator<T> iterator();
}

