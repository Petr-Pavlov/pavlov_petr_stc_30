package petr.lesson11;

public interface Iterator<T> {
    T next ();
    boolean hasNext();
}
