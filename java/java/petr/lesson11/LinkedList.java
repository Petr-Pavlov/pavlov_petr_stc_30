package petr.lesson11;

public class LinkedList<T> implements List<T> {

    private class LinkedListIterator implements Iterator<T> {

        Node<T> currentNode = first;

        @Override
        public T next() {
            T value = currentNode.value;
            currentNode = currentNode.next;
            return value;

        }

        @Override
        public boolean hasNext() {
            return currentNode.next != null;
        }
    }

    private Node<T> first;
    private Node<T> last;

    private int count;

    private static class Node<T> {
        T value;
        Node<T> next;

        public Node(T value) {
            this.value = value;
        }
    }

    @Override
    public T get(int index) {

        if (index < 0 || index >= count) {
            throw new IllegalArgumentException("Такого элемента нет");
        } else if (first != null) {
            int i = 0;
            Node<T> current = this.first;

            while (i < index) {
                current = current.next;
                i++;
            }
            return current.value;
        }
        return null;
    }

    @Override
    public int indexOf(T element) {
        int i = 0;
        Node<T> current = this.first;

        while (current != null && current.value != element) {
            current = current.next;
            i++;
        }

        if (current == null) {
            return -1;
        } else {
            return i;
        }
    }


    @Override
    public void removeByIndex(int index) {

        int i = 0;
        Node<T> current = first;
        Node<T> previous = first;

        if (index == 0) {
            first = first.next;
            count--;
        } else if (index > 0 && index < count - 1) {
            while (i < index) {
                previous = current;
                current = current.next;
                i++;
            }
            previous.next = current.next;
            count--;
        } else if (index == count - 1) {
            while (i < index) {
                current = current.next;
                i++;
            }
            last = current;
            current.next = null;
            count--;
        } else {
            System.out.println("элемента с данным индексом не существует");
        }
    }

    @Override
    public void insert(T element, int index) {
        int i = 0;
        Node<T> current = first;
        if (index == 0) {
            Node<T> toInsert = new Node<T>(element);
            toInsert.next = first;
            first = toInsert;
            count++;
        }
        if (index > 0 && index < count) {
            while (i < index - 1) {
                current = current.next;
                i++;
            }
            Node<T> following = current.next;
            Node<T> toInsert = new Node<T>(element);
            current.next = toInsert;
            toInsert.next = following;
            count++;
        }
        if (index == count){
            System.out.println("воспользуйтесь функцией add");
        }
    }


    @Override
    public void add(T element) {
        Node<T> newNode = new Node<T>(element);
        if (first == null) {
            first = newNode;
            last = newNode;
        } else {

            last.next = newNode;

            last = newNode;
        }
        count++;
    }

    @Override
    public boolean contains(T element) {
        return indexOf(element) != -1;
    }

    @Override
    public int size() {
        return count;
    }

    public void reverse() {
        T[] temp = (T[])new Object[count];
        int tempCount = count;

        for (int i = 0; i < tempCount; i ++){
            temp[i] = get(i);
        }
        first = new Node<T>(temp[tempCount - 1]);
        last = first;
        count = 1;
        for (int i = tempCount - 2; i >= 0; i--){
            add(temp[i]);
        }


    }

    @Override
    public void removeFirst(T element) {

        int i = 0;
        Node<T> current = first;
        Node<T> previous = first;

        while (current != null && current.value != element) {
            previous = current;
            current = current.next;
            i++;
        }

        if (current == null) {
            System.err.println("Элемент не найден");
        } else if (previous == first){
            first = first.next;
            count--;
        } else
            {
            previous.next = current.next;
            count--;
        }

    }


    @Override
    public Iterator<T> iterator() {
        return new LinkedListIterator();
    }
}

