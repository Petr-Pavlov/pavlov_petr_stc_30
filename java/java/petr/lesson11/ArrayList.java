package petr.lesson11;

public class ArrayList<T> implements List<T> {

    private static final int DEFAULT_SIZE = 10;

    private T data[];

    private int count;

    public ArrayList() {
        this.data = (T[])new Object[DEFAULT_SIZE];
    }

    private class ArrayListIterator implements Iterator<T> {

        private int current = 0;

        @Override
        public T next() {
            T value = data[current];
            current++;
            return value;
        }

        @Override
        public boolean hasNext() {
            return current < count;
        }
    }

    @Override
    public T get(int index) {

        if (index < count && index >= 0) {
            return this.data[index];
        }
        else {
            throw new IllegalArgumentException("элемента с данным инжексом не существует");
        }
    }


    @Override
    public int indexOf(T element) {
        for (int i = 0; i < count; i++) {
            if (data[i].equals(element)) {
                return i;
            }
        }
        return -1;
    }
    public void validateIndex (int index){
        if (index > count && index < 0) {
            throw new IllegalArgumentException("элемента с данным инжексом не существует");
        }

    }

    @Override
    public void removeByIndex(int index) {
        validateIndex(index);
            for (int i = index; i < count; i++) {
                data[i] = data[i + 1];
            }
            count--;

    }

    @Override
    public void insert(T element, int index) {
        validateIndex(index);
            for (int i = count; i > index; i--) {
                data[i] = data[i - 1];
            }
            data[index] = element;
            count++;
    }

    @Override
    public boolean contains(T element) {
        return indexOf(element) != -1;
    }

    @Override
    public void add(T element) {
        if (count == data.length - 1) {
            resize();
        }
        data[count] = element;
        count++;
    }


    private void resize() {
        int oldLength = this.data.length;

        int newLength = oldLength + (oldLength >> 1);
        T newData[] = (T[])new Object[newLength];

        System.arraycopy(this.data, 0, newData, 0, oldLength);

        this.data = newData;
    }


    @Override
    public int size() {
        return this.count;
    }

    @Override
    public void removeFirst(T element) {
        int indexOfRemovingElement = indexOf(element);

        for (int i = indexOfRemovingElement; i < count - 1; i++) {
            this.data[i] = this.data[i + 1];
        }

        this.count--;
    }


    @Override
    public Iterator<T> iterator() {
        return new ArrayListIterator();
    }
}
