package petr.lesson11;

public class MainArrayList {

    public static void main(String[] args) {
        List<Integer> list = new ArrayList<Integer>();

        for (int i = 0; i < 17; i++) {
            list.add(i);
        }
        list.insert(135,14);

        System.out.println(list.size());

        Iterator<Integer> iterator = list.iterator();

        while (iterator.hasNext()) {
            System.out.println(iterator.next());
        }


    }
}
