package petr.lesson4;

import java.util.Scanner;

/**
 * таким образом пишется документация на классы методы и поля, ее можно потом вызвать прямо в коде с помощью идеии
 * (ставишь курсор на то что тебе интересно и нажимаешь ctrl+Q)
 */
public class BinarySearcher {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Введите размер массива ");
        int n = scanner.nextInt();
        int[] array = new int[n];  // TODO DONE int[] array  - ставь квадратные скобки рядом с типом данных

        System.out.println("Введите " + array.length + " чисел для заполнения массива в порядке возрастания их значений");
        for (int i = 0; i < array.length; i++) {
            array[i] = scanner.nextInt();
        }

        boolean isSorted = true;
        for (int i = 0; i < array.length - 1; i++) {
            if (array[i] >= array[i + 1]) {
                isSorted = false;
                break;
            }
        }

        if (isSorted == false){
            System.out.println("Массив не отсортирован");
        }
        else {
            System.out.println("Введите искомое число:");
            int element = scanner.nextInt();
            int index = binarySearch(array, element, 0, array.length - 1);
            System.out.println("Индекс искомого числа равен " + index);
        }

    }


    private static int binarySearch(int[] array, int element, int minIndex, int maxIndex) {
        int result = 0;

        if (minIndex <= maxIndex) {
            int midIndex = (minIndex + maxIndex) / 2;
            if (array[midIndex] > element) {
                binarySearch(array, element, minIndex, midIndex - 1);
            } else if (array[midIndex] < element) {
                binarySearch(array, element, midIndex + 1, maxIndex);
            } else {
                result = midIndex;
            }
        } else {
            result = -1;
        }
        return result;




            //System.out.println(midIndex); TODO DONE в конечном коде этого не должно быть

    }

    /* TODO DONE "добавлена проверка" бинарный поиск работает только с отсортированными массивами, нужно
        - либо предусмотреть проверку на этот случай
        - либо сортировать самому
        - либо описать это в документации
     */

    /* TODO
        нужно поддерживать чистоту API (application program interface - сигнатуры публичных методов)
        сделай свой метод приватным, и добавь метод-обертку, который будет
        - задавать стартовые значения
        - проверять валидность массива и сортировать при необходимости (либо опиши в документации что массив должен быть отсортирован)
        чтобы пользователь твоей программы не

        public static int binarySearch(int[] array, int element) {
           return binarySearch(array, element, 0, array.length)
        }
     */

    // TODO метод должен возращать значение чтобы его можно было использовать в других участках кода
    // TODO нужно предусмотреть что будет если значение не найдено
    // TODO выводить сообщение не задача этого метода, он должен вернуть индекс, а что выводить должен решать "тот кто спрашивает"


}