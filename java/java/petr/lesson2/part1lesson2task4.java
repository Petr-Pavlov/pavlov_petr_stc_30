package petr.lesson2;

import java.util.Arrays;
import java.util.Scanner;
class Part1Lesson2Task4 {
	public static void main(String args[]) {
		Scanner scanner = new Scanner(System.in);
		int n = scanner.nextInt();
		int array[] = new int[n];

		for (int i = 0; i < array.length; i++){
			array[i] = scanner.nextInt();
		}

		int arrayMin = array[0];
		int arrayMax = array[0];
		int indexOfMin = 0;
		int indexOfMax = 0;

		for (int i = 1; i < array.length; i++){
			if (arrayMin > array[i]){
				arrayMin = array[i];
				indexOfMin = i;
			}
		}

		for (int i = 1; i < array.length; i++){
			if (arrayMax < array[i]){
				arrayMax = array[i];
				indexOfMax = i;
			}
		}

		int permanentNumber = array[indexOfMax];
		array[indexOfMax] = array[indexOfMin];
		array[indexOfMin] = permanentNumber;


		System.out.println(Arrays.toString(array));
	}
}