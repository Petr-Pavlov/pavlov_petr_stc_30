package petr.lesson2;

import java.util.Scanner;
		class Program {
		public static void main(String args[]) {
		Scanner scanner = new Scanner(System.in);
		int m = scanner.nextInt(); //lines
		int n = scanner.nextInt(); //columns

		int array[][] = new int[m][n];
		int currentNumber = 1;
		int lines = m - 1;
		int columns = n - 1;
		int round = 0;
		int ending = m*n + 1;

		while (currentNumber < ending){
			for (int i = round; i < columns; i++){
				array[round][i] = currentNumber;
				currentNumber = currentNumber + 1;
			}

			for (int i = round; i < lines; i++){
				array[i][columns] = currentNumber;
				currentNumber = currentNumber + 1;
			}

			for (int i = columns; i > round; i--){
				array[lines][i] = currentNumber;
				currentNumber = currentNumber + 1;
			}

			for (int i = lines; i > round; i--){
				array[i][round] = currentNumber;
				currentNumber = currentNumber + 1;
			}
			
			round++;
			lines--;
			columns--;
		}


		for (int i = 0; i < m; i++) {
			for (int j = 0; j < n; j++) {
				System.out.print(array[m][n] + " ");
			}
			System.out.println();
		}
	}
}