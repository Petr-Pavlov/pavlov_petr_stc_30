package petr.lesson2;

import java.util.Scanner;
class Part1Lesson2Task3 {
	public static void main(String args[]) {
		Scanner scanner = new Scanner(System.in);
		int n = scanner.nextInt();
		int array[] = new int[n];
		
		for (int i = 0; i < array.length; i++) {
			array[i] = scanner.nextInt();
		}

		int arraySum = 0;
		for (int i = 0; i < array.length; i++) {
			arraySum = arraySum + array[i];
		}

		int arrayAverage = arraySum / array.length;
		System.out.println(arrayAverage);
	}
}