package petr.lesson2;

import java.util.Scanner;
import java.util.Arrays;
class Part1Lesson2Task2 {
	public static void main(String args[]) {
		Scanner scanner = new Scanner(System.in);
		int n = scanner.nextInt();
		int firstArray[] = new int[n];
		int array [] = new int[n];
		int j = 0;

		for (int i = 0; i < firstArray.length; i++) {
			firstArray[i] = scanner.nextInt();
		}
		
		for (int i = array.length - 1; i >= 0; i--) {
			array[j] = firstArray [i];
			j++;
		}
		
		System.out.println(Arrays.toString(array));
	}
}
