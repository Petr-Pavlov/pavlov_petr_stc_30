package petr.lesson2;

class Part1Lesson2Task6 {
	public static void main(String args[]) {
		int array[] = {4, 2, 3, 5, 7};
		int number = 0;
		int numberRank = 1;
		
		for (int i = array.length - 1; i >= 0; i--){
			number = number + array[i] * numberRank;
			numberRank = numberRank * 10;
		}

		System.out.println(number); 
	}
}