package petr.lesson2;

import java.util.Arrays;
import java.util.Scanner;
class Part1Lesson2Task5 {
	public static void main(String args[]) {
		Scanner scanner = new Scanner(System.in);
		int n = scanner.nextInt();
		int array[] = new int[n];

		for (int i = 0; i < array.length; i++){
			array[i] = scanner.nextInt();
		}

		int permanentNumber = 0;
		boolean sorted = false;

		while (sorted == false){
			sorted = true;
			
			for (int i = 0; i < array.length - 1; i++){
				int j = i + 1;
				if (array[i] > array[j]){
					permanentNumber = array[j];
					array[j] = array[i];
					array[i] = permanentNumber;
					sorted = false;
				}
			}
		}

		System.out.println(Arrays.toString(array));
	}
}