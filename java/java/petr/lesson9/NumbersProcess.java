package petr.lesson9;

public interface NumbersProcess {
    int process(int number);
}
