package petr.lesson9;

import java.util.Arrays;
public class NumbersAndStringProcessor {

    String[] stringArray;
    int[] intArray;

    public NumbersAndStringProcessor(int[] intArray, String[] stringArray){
        this.intArray = intArray;
        this.stringArray = stringArray;
    }

    String[] process(StringsProcess stringProcess) {
        String[] processed = new String[stringArray.length];
        for (int i = 0; i < stringArray.length; i++) {
            processed[i] = stringProcess.process(stringArray[i]);
        }
        return processed;

    }

    int[] process(NumbersProcess numbersProcess) {
        int[] processed = new int[intArray.length];
        for (int i = 0; i < intArray.length; i++) {
            processed[i] = numbersProcess.process(intArray[i]);
        }
        return processed;
    }


}
