package petr.lesson9;

import java.util.Arrays;

public class Main {

    public static void main(String[] args) {
        String[] stringArray = {"FiRsT0", "0sEcOnD", "ThIrD0", "0fOuRtH", "Fi0FtH"};
        int[] intArray = {102030405, 6789000, 54321, 98765, 192837465};

        NumbersAndStringProcessor numbersAndStringProcessor = new NumbersAndStringProcessor(intArray, stringArray);

        String[] expandeWord = numbersAndStringProcessor.process((String word) -> {
            char[] temp = word.toCharArray();
            String expandedWord = "";
                    for (int i = temp.length - 1; i >= 0; i--) {
                        String letter = Character.toString(temp[i]);
                        expandedWord += letter;
                    }

                    return expandedWord;
                }
        );

        String[] deleteZerosFromWord = numbersAndStringProcessor.process((String word) -> {
                    String temp = word.replace("0", "");
                    return temp;
                }
        );

        String[] makeSmallLetterBig = numbersAndStringProcessor.process((String word) -> {
                    String temp = word.toUpperCase();
                    return temp;
                }
        );

        int[] expanded = numbersAndStringProcessor.process((int number) -> {
                    int expandedNumber = 0;
                    while (number != 0) {
                        expandedNumber = expandedNumber * 10;
                        expandedNumber += number % 10;
                        number = number / 10;
                    }
                    return expandedNumber;
                }
        );


        int[] withoutZeros = numbersAndStringProcessor.process((int number) -> {
            int numberWithoutZeros = 0;
            int numberRank = 1;
            while (number != 0) {
                if (number % 10 != 0) {
                    numberWithoutZeros += (number % 10) * numberRank;
                    numberRank *= 10;
                }
                number = number / 10;
            }
            return numberWithoutZeros;
        });

        int[] changeDigits = numbersAndStringProcessor.process((int number) -> {
            int numberWithChangedDigits = 0;
            int numberRank = 1;
            while (number != 0) {
                if (number % 2 == 0) {
                    numberWithChangedDigits = numberWithChangedDigits + ((number % 10) - 1) * numberRank;
                    numberRank *= 10;
                } else {
                    numberWithChangedDigits = numberWithChangedDigits + ((number % 10) - 2) * numberRank;
                    numberRank *= 10;
                }
                number = number / 10;
            }
            return numberWithChangedDigits;
        });


        System.out.println(Arrays.toString(expandeWord));
        System.out.println(Arrays.toString(deleteZerosFromWord));
        System.out.println(Arrays.toString(makeSmallLetterBig));
        System.out.println(Arrays.toString(expanded));
        System.out.println(Arrays.toString(withoutZeros));
        System.out.println(Arrays.toString(changeDigits));

    }




}
