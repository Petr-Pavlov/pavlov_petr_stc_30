package petr.lesson12;

public class Main {
    public static void main(String[] args) {
        Map<String, Integer> map = new HashMapImpl<>();

        map.put("Марсель", 26);
        map.put("Неля", 18);
        map.put("Полина", 18);
        map.put("Марсель", 234);


        System.out.println(map.get("Марсель"));
        System.out.println(map.get("Неля"));
        System.out.println(map.get("Полина"));
        System.out.println(map.get("Марсель"));

        Set<Integer> set = new HashSetImpl<>();
        set.add(1);
        set.add(2);
        set.add(3);
        System.out.println(set.contains(3));


    }
}
