package petr.lesson12;

public class HashMapImpl<K, V> implements Map<K, V> {
    private static final int DEFAULT_SIZE = 16;

    private MapEntry<K, V> entries[] = new MapEntry[DEFAULT_SIZE];

    private static class MapEntry<K, V> {
        K key;
        V value;
        MapEntry<K, V> next;

        public MapEntry(K key, V value) {
            this.key = key;
            this.value = value;
        }
    }

    @Override
    public void put(K key, V value) {
        // TODO декомпозировать (разделить на методы поменьше) так чтобы стало понятней и избежать дублирование кода (опционально)
        MapEntry<K, V> newMapEntry = new MapEntry<>(key, value);
        int index = key.hashCode() & (entries.length - 1);
        if (entries[index] == null) {
            entries[index] = newMapEntry;
        }

        else {
            MapEntry<K, V> current = entries[index];
            if (key.equals(current.key)){
                current.value = value;
            }
            else {
                while (current.next != null) {
                    if (key.equals(current.key)) {
                        current.value = value;
                    } else {
                        current = current.next;
                    }
                }
                current.next = newMapEntry;
            }
        }

    }

    @Override
    public V get(K key) {
        int index = key.hashCode() & (entries.length - 1);
        MapEntry<K, V> current = entries[index];

        while (current != null){
            if (key.equals(current.key)){
                return current.value;
            }
            else {
                current = current.next;
            }
        }
        return null;
    }
}
