package petr.lesson12;

public class HashSetImpl<V> implements Set<V> {
    private static final int DEFAULT_VALUE = 0;
    private HashMapImpl hashMapImpl = new HashMapImpl(); // TODO по хорошему мапу нужно тоже параметризировать

    @Override
    public void add(V value) {
        hashMapImpl.put(value, DEFAULT_VALUE);
    }

    @Override
    public boolean contains(V value) {
        return hashMapImpl.get(value)!= null;
    }
}
