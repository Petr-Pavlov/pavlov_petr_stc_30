package petr.lesson6;

public class Main {
    public static void main (String[] args){

        RemoteController remoteController = new RemoteController();
        TVProgram programList = new TVProgram();
        Channel channelList = new Channel(programList);
        TV tv = new TV(channelList);

        remoteController.turnOnTv(tv);

    }
}
