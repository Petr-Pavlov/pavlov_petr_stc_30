package petr.lesson6;

public class Channel {

    TVProgram programList;
    String[] channelName = new String[10]; // TODO Этому масииву место в телевизоре
    public Channel(TVProgram programList) {
        this.programList = programList;
        channelName[0] = "Первый канал";
        channelName[1] = "Второй канал";
        channelName[2] = "Третий канал";
        channelName[3] = "Четвертый канал";
        channelName[4] = "Пятый канал";
        channelName[5] = "Шестой канал";
        channelName[6] = "Седьмой канал";
        channelName[7] = "Восьмой канал";
        channelName[8] = "Девятый канал";
        channelName[9] = "Десятый канал";
    }
    // TODO String getProgramContent()
    void channelTranslated(int channelNumber){
        int temp = channelNumber - 1;
        if (temp >= 0 && temp < 10) {

            TV.translateToScreen(channelName[temp] + "   показвает передачу   " + programList.chooseProgram());

        } else {
            TV.translateToScreen("канал не найден");
        }

    }

}
