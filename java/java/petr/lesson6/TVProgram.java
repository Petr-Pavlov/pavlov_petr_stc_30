package petr.lesson6;

import java.util.Random;

public class TVProgram {
    String[] programTranslated = new String[10];
    public TVProgram(){
        programTranslated[0] = "Утренняя передача";
        programTranslated[1] = "Новости";
        programTranslated[2] = "Кулинария";
        programTranslated[3] = "Ток-шоу";
        programTranslated[4] = "Автомобили";
        programTranslated[5] = "Мультфильм";
        programTranslated[6] = "Кино";
        programTranslated[7] = "История";
        programTranslated[8] = "Вечернее шоу";
        programTranslated[9] = "Анонс";
    }

    String chooseProgram(){

        Random random = new Random();
        int i = random.nextInt(9);
        return programTranslated[i];
    }
}
