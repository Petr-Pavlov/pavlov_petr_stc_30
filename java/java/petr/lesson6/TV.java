package petr.lesson6;

public class TV {

    RemoteController remoteController; // TODO это тут не нужно: даже идея посказывает что ты этим не пользуешься
    Channel channelList;
    String translation; // TODO это тут не нужно: даже идея посказывает что ты этим не пользуешься
    int channelNumber;

    public TV (Channel channelList) {
        this.channelList = channelList;
    }

    public void turnOn(){
        translateToScreen("Выберите один из десяти каналов");
        getSignalFromRemotecontroller();
    }
    public void getSignalFromRemotecontroller(){
        channelNumber = RemoteController.sendSignalToTV();
        findChannel(channelNumber);
    }

    public static void translateToScreen(String translation){ // TODO private и не статик: метод должен относиться к конкретному объекту
        System.out.println(translation);
    }
    public void findChannel(int channelNumber){
        // TODO метод должен вернуть канал то что нашел, и ничего больше.
        channelList.channelTranslated(channelNumber);

    }


    /* TODO алгоритм работы телевизор
        получить номер канала
        найти канал по номеру
        получить из найденнного канала программу
        вывести ее на экран

     */
}
