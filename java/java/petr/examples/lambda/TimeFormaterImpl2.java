package petr.examples.lambda;

import java.util.Date;

public class TimeFormaterImpl2 implements TimeFormater {
    @Override
    public String format(Date date) {
        return date.getHours() + " : " + date.getMinutes() + " : " + date.getSeconds();
    }
}
