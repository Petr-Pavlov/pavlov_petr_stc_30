package petr.examples.lambda;

import java.util.Date;

public class TimePrinter {

    private TimeHolder timeHolder;

    public TimePrinter(TimeHolder timeHolder) {
        this.timeHolder = timeHolder;
    }

    public void printTime(Date currentTime, TimeFormater timeFormater) {
        String formatedTime = timeFormater.format(currentTime);
        System.out.println(formatedTime);
    }

    public void printTime(TimeFormater timeFormater) {
        Date currentTime = timeHolder.getCurrentTime();
        String formatedTime = timeFormater.format(currentTime);
        System.out.println(formatedTime);
    }
}
