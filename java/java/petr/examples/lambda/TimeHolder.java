package petr.examples.lambda;

import java.util.Date;

public class TimeHolder {
    public Date getCurrentTime() {
        return new Date();
    }
}
