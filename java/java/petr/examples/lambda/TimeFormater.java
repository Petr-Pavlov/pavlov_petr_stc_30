package petr.examples.lambda;

import java.util.Date;

public interface TimeFormater {
    String format(Date date);
}
