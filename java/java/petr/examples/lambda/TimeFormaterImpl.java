package petr.examples.lambda;

import java.util.Date;

public class TimeFormaterImpl implements TimeFormater {
    @Override
    public String format(Date date) {
        return date.getHours() + " " + date.getMinutes();
    }
}
