package petr.examples.lambda;

import java.util.Date;

public class Main {

    public static void main(String[] args) {
        TimeHolder timeHolder = new TimeHolder();
        TimePrinter timePrinter = new TimePrinter(timeHolder);

        TimeFormater timeFormater = new TimeFormaterImpl();
        TimeFormater timeFormater2 = new TimeFormaterImpl2();


        timePrinter.printTime(timeFormater);

        timePrinter.printTime(timeFormater2);

        timePrinter.printTime(new TimeFormater() {
            @Override
            public String format(Date date) {
                return "Now is " + date.getYear() + " year";
            }
        });

        timePrinter.printTime(current -> String.valueOf(current.getSeconds()));

    }
}
