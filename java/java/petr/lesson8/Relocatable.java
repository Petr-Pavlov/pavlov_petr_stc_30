package petr.lesson8;


public interface Relocatable {
    void relocate(double coordinateXDifference, double coordinateYDifference);
}
