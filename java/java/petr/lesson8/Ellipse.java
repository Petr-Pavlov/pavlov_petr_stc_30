package petr.lesson8;
import java.lang.Math;

public class Ellipse extends GeometricForm{

    double horizontalSize;
    double verticalSize;
    double perimeter;
    double area;

    public Ellipse(double horizontalSize, double verticalSize){
        this.horizontalSize = horizontalSize;
        this.verticalSize = verticalSize;
    }

    @Override
    public double calculatePerimeter() {
        perimeter = 4 * (Math.PI * horizontalSize * verticalSize + horizontalSize - verticalSize) / (horizontalSize + verticalSize);
        return perimeter;
    }
    @Override
    public double calculateArea() {
        area = Math.PI * horizontalSize * verticalSize;
        return area;
    }

    @Override
    public void scale() {
        System.out.println("эллипс масштабируется");
    }



}
