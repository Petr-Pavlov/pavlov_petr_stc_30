package petr.lesson8;

public abstract class GeometricForm implements Relocatable, Scalable {

    double coordinateX;
    double coordinateY;

    public abstract double calculateArea();

    public abstract double calculatePerimeter();

    @Override
    public void relocate(double coordinateXDifference, double coordinateYDifference){
        coordinateX += coordinateXDifference;
        coordinateY += coordinateYDifference;
        System.out.println("new coordinate X= " + coordinateX);
        System.out.println("new coordinate Y= " + coordinateY);
    };

    @Override
    public abstract void scale();
}
