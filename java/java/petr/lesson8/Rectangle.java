package petr.lesson8;

public class Rectangle extends GeometricForm{

    double horizontalSize;
    double verticalSize;
    double perimeter; // TODO избыточно
    double area; // TODO избыточно

    public Rectangle(double horizontalSize, double verticalSize){
        this.horizontalSize = horizontalSize;
        this.verticalSize = verticalSize;
    }

    @Override
    public double calculatePerimeter() {
        perimeter = 2 * (horizontalSize + verticalSize); // TODO избыточно
        return perimeter;
    }

    @Override
    public double calculateArea() {
        area = horizontalSize * verticalSize; // TODO избыточно
        return area;
    }

    @Override
    public void scale() {
        System.out.println("прямоугольник масштабируется");
    }


}
