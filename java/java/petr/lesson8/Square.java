package petr.lesson8;

public class Square extends Rectangle{

    public Square(double onlySize){
        super(onlySize, onlySize);
    }

    @Override
    public void scale() {
        System.out.println("квадрат масштабируется");
    }
}
