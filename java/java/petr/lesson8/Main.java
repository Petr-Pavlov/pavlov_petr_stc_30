package petr.lesson8;

import java.util.Objects;

public class Main {
    public static void main(String[] args){
        GeometricForm ellipse = new Ellipse(10,5);
        GeometricForm round = new Round (10);
        Rectangle rectangle = new Rectangle (10, 5);
        Square square = new Square (10);
        printRoundArea((Round) round);
        printAbstractArea(rectangle);
        printAbstractArea(ellipse);

        scale(rectangle);
        scale(square);
    }

    public static void printRoundArea(Round round) {
        System.out.println(round.calculateArea());
    }

    public static void printAbstractArea(GeometricForm geometricForm) {
        System.out.println(geometricForm.calculateArea());
    }


    public static void scale(Scalable scalable) {
        scalable.scale();
    }
}
