package example;


public class PolimophismExample {

    public static void main(String[] args) {

        Employ[] employs = new Employ[5];

        Progger petr = new Progger();
        employs[0] = new Buhgalter();
        employs[1] = petr;
        employs[2] = new Employ();
        employs[3] = new Progger();
        employs[4] = new Employ();

        for (Employ employ : employs) {
            employ.work();
        }

        petr.deployProject();

    }


}

class Employ {

    void work(){
        System.out.println("I am working");
    }
}

class Buhgalter extends Employ {

    void work(){
        System.out.println("I am counting money");
    }
    void makeReport(){
        System.out.println("I made year report");
    }
}

class Progger extends Employ {
    void work() {
        System.out.println("I am coding");
    }
    void deployProject(){
        System.out.println("I am deploing project to server");
    }
}


